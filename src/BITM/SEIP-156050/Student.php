<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/22/2017
 * Time: 7:25 PM
 */

namespace App;


class Student
{
    private $name;
    private $studentID;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}