<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>information gatering form</title>
    <link rel="stylesheet" href="../../resoures/bootstrap/css/bootstrap.min.css" >
    <link rel="stylesheet" href="../../resoures/bootstrap/css/bootstrap-theme.min.css" >
    <link rel="stylesheet" href="../../resoures/style.css">

    <script src="../../resoures/bootstrap/js/bootstrap.min.js" ></script>
</head>
<body>
<div class="container">
    <form action="process.php" method="post">
        <div class="studentInformation">
            <div class="form-group">
                <label for="name">Student's Name:</label>
                <input type="text" class="form-control" name="name" placeholder="please enter student's name here">
                </div>

            <div class="form-group">
                <label for="studentId">Student's ID:</label>
                <input type="text" class="form-control" name="studentID" placeholder="please enter student's ID">

            </div>
        </div>
        <div class="resultInformation">
            <div class="form-group">
                <label for="markBangla">Bangla Mark:</label>
                <input type="number" step="any" min="0" max="100" class="form-control" name="markBangla" placeholder="enter bangla mark here">
            </div>
            <div class="form-group">
                <label for="markEnglish">English Mark:</label>
                <input type="number" step="any" min="0" max="100" class="form-control" name="markEnglish" placeholder="enter english mark here">
            </div>
            <div class="form-group">
                <label for="markMath">Bangla Mark:</label>
                <input type="number" step="any" min="0" max="100" class="form-control" name="markMath" placeholder="enter math mark here">
            </div>
            </div>
            <button type="submit" class="btn btn-lg btn-primary btn-block" name="submit">Submit</button>
        </div>
    </form>
</div>
</body>
</html>